<img src="https://help.veracode.com/internal/api/webapp/header/logo" width="200" /><br>  
  
# Pipeline-Templates
  
## Overview

GitLab yml templates to run Veraocde static, sca and dynamic analysis. as well create GitLab issues. Thees templates can be included on other pipelines without the need to dublicate the work in setting those pipelines up individually.  
These pre-configured template will help to scale application security testing through your entire application/repository/pipeline landscape.  
You only need to decide when to run which of these scans on your individual pipelines.  
  
## Veracode-Scanning tamplates  

### Static analysis - SAST  
Veracode offers 3 types of static analysis  
1. Pipeline Scan 
2. Sandbox Scan  
3. Policy Scan  
  
All 3 are run on different needs and have different purposes, all are based on the same engine in the background and all 3 should find the exact same results if the same binary is uploaded.  
  
### Pipeline Scan
`veracode_sast_pipeline_scan.yml`
```yml
.veracode_sast_pipeline_scan:  
      image: veracode/pipeline-scan:latest  
      script:  
          - java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --request_policy $VERACODE_POLICYNAME 2>&1 | tee pipeline_scan_policy_output.txt  
          - java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --file $VERACODE_FILEPATH --issue_details true   --gl_issue_generation true --gl_vulnerability_generation true --policy_file $VERACODE_POLICYNAME.json -bf $VERACODE_BASELINE_FILENAME -fjf filtered_results.json 2>&1 | tee pipeline_scan_text_output.txt  
      artifacts:  
          reports:   
              sast: veracode_gitlab_vulnerabilities.json  
          paths:  
              - results.json  
              - filtered_results.json  
              - pipeline_scan_text_output.txt  
              - pipeline_scan_policy_output.pipeline_scan_text_output  
              - veracode_gitlab_vulnerabilities.json  
          when: always  
          name: "veracode-pipeline-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"  
      allow_failure: true  
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${VERACODE_API_ID}`
- `${VERACODE_API_KEY}`
- `$VERACODE_POLICYNAME`
- `$VERACODE_BASELINE_FILENAME`  
  
GitLab Variables  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`
  
**The yml explained in detail**
  
This yaml template is using the Veracode provided docker image `veracode/pipeline-scan:latest`. Using this docker image doesn’t require to always download the pipeline scan jar file with every pipeline run and also is using the docker built in process to auto-update the docker image if there is a new version available.  
Veracode provided docker containers cma be found on [Dockerhub](https://hub.docker.com/search?q=veracode&type=image)  
The full documentation can be found here: [User Veracode Docker Images](https://docs.veracode.com/r/Use_Veracode_Docker_Images)  
  
**`image: veracode/pipeline-scan:latest`**
  
The main script is doing 2 things  
- downloading a policy file  
- running the pipeline scan  
  
This will download the policy file for the policy called “Veradem Policy”. It is used here to show this functionality but also I want to report on policy relevant findings only. Therefore developers know very early what they need to fix in order to achieve policy compliance later on.  
 **`- java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --request_policy $VERACODE_POLICYNAME 2>&1 | tee pipeline_scan_policy_output.txt`**   
  
The second java command will run the pipeline scan with a few flags.  
**`- java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --file $VERACODE_FILEPATH --issue_details true   --gl_issue_generation true --gl_vulnerability_generation true --policy_file $VERACODE_POLICYNAME.json -bf $VERACODE_BASELINE_FILENAME -fjf filtered_results.json 2>&1 | tee pipeline_scan_text_output.txt`**  
  
The full documentation for all possible pipeline-scan flags can be found here: [Pipeline Scan Command Parameters](https://docs.veracode.com/r/r_pipeline_scan_commands).  
  
This will enable the long notation of a finding.  
**`--issue_details true`**  
  
This will generate GitLab issues. Be careful as this will generate a report on all pipeline scan findings not sorted out by the baseline file.  
**`--gl_issue_generation true`**  
![GitLab Issue Generation](PipelineScanIssues.png)
  
This will generate a pipeline SAST security scan report. Be careful as this will generate a report on all pipeline scan findings not sorted out by the baseline file.  
**`--gl_vulnerability_generation true`**
![GitLab Pipeline-Scan pipeline reprot](PipelineScanReport.png)  
  
Using this, previously downloaded policy file to rate the findings accordingly  
**`--policy_file $VERACODE_POLICYNAME.json`**  
  
Using this baseline file in order to sort out all previously identified findings and only report on net-new findings for this single commit of a single developer.  
**`-bf $VERACODE_BASELINE_FILENAME`**  
  
The VERACODE_API_ID and VERACODE_API_KEY are generated on the Veracode platform and stored on the CD/CD variables as secrets.  
**`-vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY}`**  
  
In order to generate the above mentioned report the json output needs to be stored as a SAST artifact.  
```yaml
      artifacts:  
          reports:   
              sast: veracode_gitlab_vulnerabilities.json  
```  
  
### Sandbox Scan  
`veracode_sast_sandbox_scan.yml`  
```yml
.veracode_sast_sandbox_scan:
    image: veracode/api-wrapper-java
    script:
        - java -jar /opt/veracode/api-wrapper.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY}
          -action UploadAndScan -appname $CI_PROJECT_PATH_SLUG -createprofile true -autoscan true -sandboxname $CI_COMMIT_REF_NAME -createsandbox true
          -filepath $VERACODE_FILEPATH -version "Job ${CI_JOB_ID} in pipeline ${CI_PIPELINE_ID}" -scantimeout 15 2>&1 | tee sandbox_scan_output.txt
    artifacts:
        paths:
            - sandbox_scan_output.txt
        when: always
        name: "veracode-SANDBOX-SCAN-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: true
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${VERACODE_API_ID}`
- `${VERACODE_API_KEY}`
- `$VERACODE_FILEPATH`  
  
GitLab Variables used  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`
- `$CI_PROJECT_PATH_SLUG`
- `${CI_JOB_ID}`
- `${CI_PIPELINE_ID}`
  
**The yml explained in detail**  
  
This task is using the Veracode provided docker image `veracode/api-wrapper-java` to give you access to our java wrapper and you don’t need to download the wrapper everytime. Using this docker image doesn’t require to always download the pipeline scan jar file with every pipeline run and also is using the docker built in process to auto-update the docker image if there is a new version available.  
  
Veracode provided docker containers cma be found on [Dockerhub](https://hub.docker.com/search?q=veracode&type=image)  
The full documentation can be found here: [User Veracode Docker Images](https://docs.veracode.com/r/Use_Veracode_Docker_Images)  
  
**`image: veracode/api-wrapper-java`**

The main script will use our API wrapper in very standard way and kick of a scan on the platform  

**`- java -jar /opt/veracode/api-wrapper.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY}
          -action UploadAndScan -appname $CI_PROJECT_PATH_SLUG -createprofile true -autoscan true -sandboxname $CI_COMMIT_REF_NAME -createsandbox true
          -filepath $VERACODE_FILEPATH -version "Job ${CI_JOB_ID} in pipeline ${CI_PIPELINE_ID}" -scantimeout 15 2>&1 | tee sandbox_scan_output.txt`**
  
The full documentation about all API wrapper commands can be found here: [Install and user the Java API Wrapper](https://docs.veracode.com/r/t_working_with_java_wrapper)  
  
### Policy Scan  
`veracode_sast_policy_scan.yml`  
```yml
.veracode_sast_policy_scan:
    image: veracode/api-wrapper-java
    script:
        - java -jar /opt/veracode/api-wrapper.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY}
          -action UploadAndScan -appname $CI_PROJECT_PATH_SLUG -createprofile false -autoscan true
          -filepath $VERACODE_FILEPATH -version "Job ${CI_JOB_ID} in pipeline ${CI_PIPELINE_ID}" 
          -scantimeout 15 2>&1 | tee policy_scan_output.txt
    artifacts:
        paths:
            - policy_scan_output.txt
        when: always
        name: "veracode-POLICY-SCAN-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: true
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${VERACODE_API_ID}`
- `${VERACODE_API_KEY}`
- `$VERACODE_FILEPATH`  
  
GitLab Variables used  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`
- `$CI_PROJECT_PATH_SLUG`
- `${CI_JOB_ID}`
- `${CI_PIPELINE_ID}`
  
**The yml explained in detail**  
  
This task is using the Veracode provided docker image `veracode/api-wrapper-java` to give you access to our java wrapper and you don’t need to download the wrapper everytime. Using this docker image doesn’t require to always download the pipeline scan jar file with every pipeline run and also is using the docker built in process to auto-update the docker image if there is a new version available.
  
Veracode provided docker containers cma be found on [Dockerhub](https://hub.docker.com/search?q=veracode&type=image)  
The full documentation can be found here: [User Veracode Docker Images](https://docs.veracode.com/r/Use_Veracode_Docker_Images)  
**`image: veracode/api-wrapper-java`**
  
The main script will use our API wrapper in very standard way, kicks of a scan on the platform and waits for the scan to complete. This is done here as you may want to break the pipeline at this stage and use the policy compliance level as a gate to production.  
**`- java -jar /opt/veracode/api-wrapper.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY}
          -action UploadAndScan -appname $CI_PROJECT_PATH_SLUG -createprofile false -autoscan true
          -filepath $VERACODE_FILEPATH -version "Job ${CI_JOB_ID} in pipeline ${CI_PIPELINE_ID}" 
          -scantimeout 15 2>&1 | tee policy_scan_output.txt`**  
  
The full documentation about all API wrapper commands can be found here: [Install and user the Java API Wrapper](https://docs.veracode.com/r/t_working_with_java_wrapper)  


### Software composition analysis - SCA  
Veracode offers 2 types of Software Composition Analysis (SCA) scans.  
1. Scan 3rd party components of your application (libraries, frameworks and similar)  
2. Scan OS componetns of a docker containter  
  
Both may run on the same stage, but need to be triggered individually.

### Application Scan
`veracode_sca_application_scan.yml`  
```yml
.veracode_sca_application_scan:
    image: maven:3.6.0-jdk-8
    before_script:
        - curl -sL https://deb.nodesource.com/setup_17.x | bash -
        - apt-get update && apt-get -y install nodejs
        - npm install axios
        - npm install mathjs
        - git clone -b "main" --depth 50 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/veracode-gitlab-manual/veracode-helpers.git
    script:
        - curl -sSL https://download.sourceclear.com/ci.sh | bash -s scan . --update-advisor --json scaResults.json --allow-dirty --scan-collectors maven 2>&1 | tee sca_output.txt
    after_script:
        - nodejs ./veracode-helpers/Veracode-SCA-Results/dependencies.js ${PRIVATE_TOKEN} true ${CI_PROJECT_ID}
    artifacts:
        reports:
            dependency_scanning: output-sca-vulnerabilites.json
        paths:
            - sca_output.txt
            - output-sca-vulnerabilites.json
        when: always
        name: "veracode-SCA-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: true
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${SRCCLR_API_TOKEN}`  
  
GitLab Variables used  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`
- `${PRIVATE_TOKEN}`
- `${CI_PROJECT_ID}`
  
**The yml explained in detail**  
  
It is based on a maven docker image as Agent based SCA needs the package manager (maven) in the background in order to perform the scan
`image: maven:3.6.0-jdk-8`

It includes a community project (SCAResultsReport - https://gitlab.com/julz0815/scaresultsreport) in order to generate a dependency vulnerability overview using the pipeline “security” tab, as well it will generate issues based on the SCA findings. Therefore it is using a before_script and an after_script. The project is also part of this documentation at [Veracode Helpers](https://gitlab.com/veracode-gitlab-manual/veracode-helpers).  
  
This part will set up node on the maven docker image as the SCAResultsReport project is based on JavaScript. As well it will install 2 required node modules in order to run correctly.  
`before_script:
        - curl -sL https://deb.nodesource.com/setup_12.x | bash -
        - apt-get update && apt-get -y install nodejs
        - npm install axios
        - npm install mathjs` 
  
The after_script will run the actual task to generate the reports once the SCA scan is finished.  
`after_script:
        - nodejs ./veracode-helpers/Veracode-SCA-Results/dependencies.js ${PRIVATE_TOKEN} true ${CI_PROJECT_ID}` 
  
The actual script will download the agent and run it with a few flags  
`script:
        - curl -sSL https://download.sourceclear.com/ci.sh | bash -s scan . --update-advisor --json scaResults.json --allow-dirty --scan-collectors maven 2>&1 | tee sca_output.txt` 
  
A few of the paramaters explained, the full documentation of the SCA Agent Based scan can be found here: [Agent Based Scanning Scan Directives](https://docs.veracode.com/r/c_sc_scan_directives)  
  
To get a bit more information about the results  
`--update-advisor` 
  
Output in json format to be used with the report generation  
`--json scaResults.json` 
  
As I usually have uncommitted changes in my repository  
`--allow-dirty` 
  
As the SCAResultsReport is based on JavaScript and will put a package.json file into the repository, but is never published to production we don’t need to scan the JavaScript part and only focus on the real application maven part.  
`--scan-collectors maven` 
  
The script will pick up the SRCCLR_API_TOKEN token generated via the Veracode platform and stored as a secret on Gitlab.  
  
The artifact generated by SCAResultsReport will be stored for dependency_scanning to build the pipeline report.  
`artifacts:
        reports:
            dependency_scanning: output-sca-vulnerabilites.json` 
  
![ScanReport](ABSAppScanReport.png)  
  
### Docker Scan  
`veraocde_sca_docker_scan.yml`  
```yml
.veracode_sca_docker_scan:
    image: juliantotzek/dockercli
    services:
        - docker:dind
    variables:
        DOCKER_HOST: tcp://docker:2375
    script:
        - curl -sSL https://download.sourceclear.com/ci.sh | bash -s scan --image $DOCKER_IMAGE_NAME scaResults.json 2>&1 | tee sca_docker_output.txt
    artifacts:
        paths:
            - sca_docker_output.txt
        when: always
        name: "veracode-SCA-DOCKER-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: false
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${SRCCLR_API_TOKEN}`  
  
GitLab Variables used  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`  
  
**The yml explained in detail**  
  
It is based on a maven docker image as Agent based SCA needs the package manager (maven) in the background in order to perform the scan
`juliantotzek/dockercli`
  
The actual script will download the agent and run it with a few flags  
`script:
        - curl -sSL https://download.sourceclear.com/ci.sh | bash -s scan --image $DOCKER_IMAGE_NAME scaResults.json 2>&1 | tee sca_docker_output.txt'   
  
The full documentation of the SCA Agent Based scan can be found here: [Agent Based Scanning Scan Directives](https://docs.veracode.com/r/c_sc_scan_directives)  

This task can also use a community project (SCAResultsReport - https://gitlab.com/julz0815/scaresultsreport) in order to generate a dependency vulnerability overview using the pipeline “security” tab, as well it will generate issues based on the SCA findings. Therefore it is using a before_script and an after_script. The project is also part of this documentation at [Veracode Helpers](https://gitlab.com/veracode-gitlab-manual/veracode-helpers).  

### Dynamic Analysis  
`veracode_dast_rescan_yml`  
```yml
.veracode_dast_rescan:
    image: veracode/api-signing
    before_script:
        - export VERACODE_API_KEY_ID=${VERACODE_API_ID}
          export VERACODE_API_KEY_SECRET=${VERACODE_API_KEY}
    script:
        - http --auth-type veracode_hmac PUT https://api.veracode.com/was/configservice/v1/analyses/4e1ef3bc1b061f9e4331b0af10dc8138?method=PATCH < da_scan_update.json
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${VERACODE_API_ID}`
- `${VERACODE_API_KEY}`  
  
**The yml explained in detail**  
  
It is using the Veracode provided docker image `veracode/ api-signing:latest`. Using this docker image doesn’t require to always install the Veracode signing HMAC authentication library with every pipeline run and also is using the docker built in process to auto-update the docker image if there is a new version available  
`image: veracode/api-signing`
  
Push the API credentidal that are stored on the CI/CD settings as variables to the running task  
`variables:
        VERACODE_API_KEY_ID=${VERACODE_API_ID}
        VERACODE_API_KEY_SECRET=${VERACODE_API_KEY}`  
  
The main script is using a standard API call to reconfigure an already set up dynamic analysis scan on the platform, setting up a new schedule for it. It is using a json file that is stored on the repository, `da-scan_update.json`.  
`script:
        - http --auth-type veracode_hmac PUT https://api.veracode.com/was/configservice/v1/analyses/4e1ef3bc1b061f9e4331b0af10dc8138?method=PATCH < da_scan_update.json`  

The full documentation about what needs to be send within the JSON file can be found here: [Dynamic Analysis APIs](https://docs.veracode.com/r/orRWez4I0tnZNaA_i0zn9g/ca9kKngOeZvMUsWlyhXJLw)  

## Veracode-Reporting templates  
The reporting templates will allow you to create GitLab issues from your static and SCA scans.  
  
### Static Analysis  
`veracode_sast_policy_scan_reporting.yml`
```yml
.veracode_sast_policy_scan_reporting:
    image: node:latest
    before_script:
        - npm ci
        - git clone -b "main" --depth 50 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/veracode-gitlab-manual/veracode-helpers.git
    script:
        - cd veracode-helpers/Veracode-Static-Results
        - npm run results-import scan_type=policy profile_name=$CI_PROJECT_PATH_SLUG gitlab_token=${PRIVATE_TOKEN} gitlab_project=$GITLAB_PROJECT_ID create_issue=true  
    artifacts:
        reports:
            sast: ./veracode-helpers/Veracode-Static-Results/output-sast-vulnerabilites.json
        paths: 
            - ./veracode-helpers/Veracode-Static-Results/output-sast-vulnerabilites.json
        when: always
        name: "veracode-POLICY-SCAN-RESULTS-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${VERACODE_API_ID}`
- `${VERACODE_API_KEY}`  
  
GitLab Variables used  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`
- `$CI_PROJECT_PATH_SLUG`
- `$GITLAB_PROJECT_ID`  
  
**The yml explained in detail**  

This task makes use of a community project (VeracodeSASTResultsImport - https://gitlab.com/julz0815/veracodesastresultsimport) to generate a SAST pipeline security scan report as well can generate GitLab issues from a sandbox scan. The project is also part of this documentation at [Veracode Helpers](https://gitlab.com/veracode-gitlab-manual/veracode-helpers)  
  
This will install the JavaScript application on the used image  
`before_script:
        - npm ci`
  
The main script will generate the report and creates Gitlab issues if configured to do so  
`script:
        - npm run results-import scan_type=policy profile_name=Verademo gitlab_token=${PRIVATE_TOKEN} gitlab_project=17140954 create_issue=true`  
  
Artifacts need to be stored as a SAST security report  
`artifacts:
        reports:
            sast: output-sast-vulnerabilites.json` 
  
![GitLab Sandbox-Scan pipeline reprot](SandboxScanPipelineReport.png)  
  
`veracode_sast_sandbox_scan_reporting.yml`  
```yml
.veracode_sast_sandbox_scan_reporting:
    image: node:latest
    before_script:
        - git clone -b "main" --depth 50 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/veracode-gitlab-manual/veracode-helpers.git
        - cd veracode-helpers/Veracode-Static-Results
        - npm ci
    script:
        - npm run results-import scan_type=sandbox profile_name=$CI_PROJECT_PATH_SLUG sandbox_name=$CI_COMMIT_REF_NAME gitlab_token=${PRIVATE_TOKEN} gitlab_project=${CI_PROJECT_ID} create_issue=true  
    artifacts:
        reports:
            sast: veracode-helpers/Veracode-Static-Results/output-sast-vulnerabilites.json
        paths: 
            - veracode-helpers/Veracode-Static-Results/output-sast-vulnerabilites.json
        when: always
        name: "veracode-POLICY-SCAN-RESULTS-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${VERACODE_API_ID}`
- `${VERACODE_API_KEY}`  
  
GitLab Variables used  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`
- `$CI_PROJECT_PATH_SLUG`
- `$GITLAB_PROJECT_ID`
  
**The yml explained in detail**  
  
This task makes use of a community project (VeracodeSASTResultsImport - https://gitlab.com/julz0815/veracodesastresultsimport) to generate a SAST pipeline security scan report as well can generate GitLab issues from a sandbox scan. The project is also part of this documentation at [Veracode Helpers](https://gitlab.com/veracode-gitlab-manual/veracode-helpers)  
  
This will install the JavaScript application on the used image  
`before_script:
        - npm ci` 
          
The main script will generate the report and creates Gitlab issues if configured to do so  
`script:
        - npm run results-import scan_type=sandbox profile_name=Verademo sandbox_name=gitlab-release gitlab_token=${PRIVATE_TOKEN} gitlab_project=17140954 create_issue=true`  
  
Artifacts need to be stored as a SAST security report  
`artifacts:
        reports:
            sast: output-sast-vulnerabilites.json`  
  
![GitLab Sandbox-Scan pipeline reprot](SandboxScanPipelineReport.png)  
  
### Working with the Pipeline-Scan  
  
`veracode_pipeline_scan_create_new_baseline_file` 
  
```yml
.veracode_pipeline_scan_create_new_baseline_file:
    image: veracode/pipeline-scan:latest
    script:
        - java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --file $VERACODE_FILEPATH -jf $VERACODE_BASELINE_FILEPATH 2>&1 | tee pipeline_baseline_file_output.txt
    artifacts:
        paths:
            - $VERACODE_BASELINE_FILEPATH
            - pipeline_baseline_file_output.txt
        when: always
        name: "veracode-pipeline-baseline-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: true
```

**Variables you need to pass to this yml pipeline**  
individual variables:  
- `${VERACODE_API_ID}`
- `${VERACODE_API_KEY}`  
- `$VERACODE_FILEPATH`
- `$VERACODE_BASELINE_FILEPATH`  
  
GitLab Variables used  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`
  
**The yml explained in detail**  
  
This task "simply" runs another Veracode Pipeline-Scan to generate a basline file from the findings. Using this process it will be possible to sort out previous findings and only report on net-net findings on future Pipeline-Scans scans. It is using the Veracode provided docker image `veracode/pipeline-scan:latest`. Using this docker image doesn’t require to always install the Veracode signing HMAC authentication library with every pipeline run and also is using the docker built in process to auto-update the docker image if there is a new version available  
`veracode/pipeline-scan:latest`
  
The main script will run the pipeline-scan scan.    
`script:
        - java -jar /opt/veracode/pipeline-scan.jar -vid ${VERACODE_API_ID} -vkey ${VERACODE_API_KEY} --file $VERACODE_FILEPATH -jf $VERACODE_BASELINE_FILEPATH 2>&1 | tee pipeline_baseline_file_output.txt` 
  
The output will be stored as an artifact on this pipeline run.  
`artifacts:
        paths:
            - pipeline-basline.json`  
  

`.veracode_pipeline_scan_baseline_file_commit` 
  
```yml
.veracode_pipeline_scan_baseline_file_commit:
    before_script:
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval $(ssh-agent -s)
        - ssh-add <(echo "$GIT_SSH_PRIV_KEY")
        - git config --global user.name "${GITLAB_USER_NAME}"
        - git config --global user.email "${GITLAB_USER_EMAIL}"
        - mkdir -p ~/.ssh
        - cat gitlab-known-hosts >> ~/.ssh/known_hosts
    script:
        - git --help
        - git add -f $VERACODE_BASELINE_FILEPATH
        - git commit -m "Pipeline Baseline from $CI_COMMIT_SHORT_SHA" || echo "No changes, nothing to commit!"
        - git remote rm origin && git remote add origin git@gitlab.com:$CI_PROJECT_PATH.git
        - git push origin HEAD:$VERACODE_BRANCH_NAME
    allow_failure: true  
```
**Variables you need to pass to this yml pipeline**  
individual variables:  
- `$VERACODE_BASELINE_FILEPATH`  
- `$VERACODE_BRANCH_NAME` 
  
GitLab Variables used  
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_SHORT_SHA`  
- `$GIT_SSH_PRIV_KEY` 
- `${GITLAB_USER_NAME}` 
- `${GITLAB_USER_EMAIL}`  
- `$CI_PROJECT_PATH`  
  
**The yml explained in detail**  
  
This task will commit a generated Pipeline-Scan baseline file back to a specifed repostiory and branch. This allows you to use the baseline file for future Pipeline-Scan scans and sort out all previsous fidnings, essentially reporting on only net-new findigns from a specific commit.
  
The before and main script may need to be adjusted to fit your very specific environment. It gives a quick overview how a file can be committed back to a repository and branch.    
  
The before_script and script part of this step requires quite a bit of configuration. I followed this tutorial https://about.gitlab.com/blog/2017/11/02/automating-boring-git-operations-gitlab-ci/ to set it up. Essentially there need to be SSL certificate login be enabled on your repository, the certificate needs to be stored within the GitLab secrets and will be picked up by the script. In addition you need to create a “known-hosts” file, stored on your repository, that OpenSSL is allowed to access the gitlab domain. The main script are basic git commands to commit the generated pipeline-baseline.json file back to the development branch of the repository.  
  
A known-hosts file looks like this and is based on the GitLab cloud version gitlab.com. It needs to be generated for the customers GitLab domain.  
  
```
gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9
gitlab.com ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=
gitlab.com ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf
```  
  
